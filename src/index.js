import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import MarkdownEditor from "./components/MarkdownEditor";

ReactDOM.render(
  <MarkdownEditor useDark={true} />,
  document.getElementById("root")
);
