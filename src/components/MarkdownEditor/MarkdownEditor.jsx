import classnames from "classnames";
import React, { useState, useEffect } from "react";
import styles from "./MarkdownEditor.module.scss";
import marked from "marked";
import hljs from "highlight.js";
import "highlight.js/styles/monokai.css";
import {
  FaSave,
  FaBold,
  FaItalic,
  FaQuoteLeft,
  FaLink,
  FaImage,
  FaListOl,
  FaList,
  FaCode,
  FaAdjust,
  FaUndo,
  FaRedo,
} from "react-icons/fa";

marked.setOptions({
  breaks: true,
  highlight: (code, lang) => {
    if (lang) {
      const validLanguage = hljs.getLanguage(lang) ? lang : "plaintext";
      return hljs.highlight(validLanguage, code).value;
    }
    return hljs.highlightAuto(code).value;
  },
});

const renderer = new marked.Renderer();
renderer.link = (href, title, text) =>
  `<a target="_blank" href="${href}">${text}</a>`;

const Separator = () => <div className={styles.separator}></div>;

const Header = ({
  textValue,
  setTextValue,
  titleValue,
  setTitleValue,
  darkMode,
  setDarkMode,
}) => {
  const handleDownload = () => {
    if (titleValue) {
      // retain line breaks
      const text = textValue.replace(/\n/g, "\r\n");
      const title =
        titleValue.replace(/[^a-z0-9]/gi, "-").toLowerCase() + ".md";
      const blob = new Blob([text], { type: "text/plain" });
      const url = window.URL.createObjectURL(blob);
      let a = document.createElement("a");
      a.href = url;
      a.download = title;
      a.click();
    } else {
      alert("Please, type document title");
    }
  };

  const handleItalic = (e) => handleMarkdown(e, "italic");
  const handleMono = (e) => handleMarkdown(e, "mono");
  const handleBold = (e) => handleMarkdown(e, "bold");
  const handleQuote = (e) => handleMarkdown(e, "quote");
  const handleCode = (e) => handleMarkdown(e, "code");
  const handleList = (e) => handleMarkdown(e, "ul");
  const handleOList = (e) => handleMarkdown(e, "ol");

  const handleLink = (e) => {
    const target = document.getElementsByTagName("textarea").item(0);
    const selStart = target.selectionStart;
    const selEnd = target.selectionEnd;
    const prefix = textValue.slice(0, selStart);
    const middle = textValue.slice(selStart, selEnd);
    const suffix = textValue.slice(selEnd);
    setTextValue(prefix + `[${middle ? middle : "link"}](#)` + suffix);
    document.getElementsByTagName("textarea").item(0).focus();
  };

  const handleImage = (e) => {
    const target = document.getElementsByTagName("textarea").item(0);
    const selStart = target.selectionStart;
    const selEnd = target.selectionEnd;
    const prefix = textValue.slice(0, selStart);
    const middle = textValue.slice(selStart, selEnd);
    const suffix = textValue.slice(selEnd);
    setTextValue(prefix + `![${middle ? middle : "alt"}](#)` + suffix);
    document.getElementsByTagName("textarea").item(0).focus();
  };

  const handleMarkdown = (e, func) => {
    let preSym = "";
    let posSym = "";
    let Sym = "";
    switch (func) {
      case "bold":
        Sym = "**";
        preSym = "**";
        posSym = "**";
        break;
      case "italic":
        Sym = "_";
        preSym = "_";
        posSym = "_";
        break;
      case "mono":
        Sym = "`";
        preSym = "`";
        posSym = "`";
        break;
      case "quote":
        Sym = "> ";
        preSym = "\n> ";
        posSym = "";
        break;
      case "code":
        Sym = "```";
        preSym = "\n```\n";
        posSym = "```";
        break;
      case "ol":
        Sym = " 1. ";
        preSym = "\n 1. ";
        break;
      case "ul":
        Sym = " - ";
        preSym = "\n - ";
      default:
        break;
    }
    const target = document.getElementsByTagName("textarea").item(0);
    const selStart = target.selectionStart;
    const selEnd = target.selectionEnd;
    const prefix = textValue.slice(0, selStart);
    const middle = textValue.slice(selStart, selEnd);
    const suffix = textValue.slice(selEnd);
    setTextValue(
      prefix +
        (middle.indexOf(Sym) !== -1
          ? middle.replaceAll(Sym, "")
          : preSym + middle + posSym) +
        suffix
    );
    document.getElementsByTagName("textarea").item(0).focus();
  };

  return (
    <header className={styles.editorHeader}>
      <input
        type="text"
        className="title"
        placeholder="Document title"
        value={titleValue}
        onChange={(e) => setTitleValue(e.target.value)}
      />
      <button onClick={handleDownload} ariaLabel="save">
        <FaSave />
      </button>
      <Separator />
      <button onClick={handleBold} ariaLabel="bold">
        <FaBold />
      </button>
      <button onClick={handleItalic} ariaLabel="italic">
        <FaItalic />
      </button>
      <button onClick={handleMono} ariaLabel="monospace">
        M
      </button>
      <Separator />
      <button onClick={handleQuote} ariaLabel="quote">
        <FaQuoteLeft />
      </button>
      <button onClick={handleLink} ariaLabel="link">
        <FaLink />
      </button>
      <button onClick={handleImage} ariaLabel="image">
        <FaImage />
      </button>
      <button onClick={handleOList} ariaLabel="ordered list">
        <FaListOl />
      </button>
      <button onClick={handleList} ariaLabel="unordered list">
        <FaList />
      </button>
      <button onClick={handleCode} ariaLabel="code">
        <FaCode />
      </button>
      <Separator />
      <button>
        <FaUndo ariaLabel="undo" />
      </button>
      <button>
        <FaRedo ariaLabel="redo" />
      </button>
      <Separator />
      <button onClick={() => setDarkMode(!darkMode)} ariaLabel="dark">
        <FaAdjust />
      </button>
    </header>
  );
};

const Editor = ({
  textValue,
  setTextValue,
  titleValue,
  setTitleValue,
  darkMode,
}) => {
  return (
    <div className={classnames(darkMode ? styles.dark : "", styles.editor)}>
      <textarea
        className={styles.text}
        value={textValue}
        onChange={(e) => setTextValue(e.target.value)}
      />
    </div>
  );
};

const Preview = ({ content, darkMode }) => (
  <div
    className={classnames(darkMode ? styles.dark : "", styles.previewWrapper)}
  >
    <div
      dangerouslySetInnerHTML={{
        __html: marked(content, { renderer: renderer }),
      }}
      className={styles.preview}
    />
  </div>
);

const Footer = () => <footer className={styles.footer}></footer>;

const MarkdownEditor = ({ useDark }) => {
  const [textValue, setTextValue] = useState("");
  const [titleValue, setTitleValue] = useState("");
  const [darkMode, setDarkMode] = useState(useDark);
  return (
    <div className={styles.markdownEditor}>
      <Header
        {...{
          textValue,
          setTextValue,
          titleValue,
          setTitleValue,
          darkMode,
          setDarkMode,
        }}
      />
      <main className={styles.mainWrapper}>
        <Editor
          darkMode={darkMode}
          {...{ textValue, setTextValue, titleValue, setTitleValue }}
        />
        <Preview darkMode={darkMode} content={textValue} />
      </main>
      <Footer />
    </div>
  );
};

export default MarkdownEditor;
